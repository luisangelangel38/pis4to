from .models import EmergencyPlan  
from django.urls import reverse
from .forms import EmergencyPlanForm
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
import folium
from django.http import HttpResponseRedirect
from myApp.models import *
import networkx as nx
from django.http import JsonResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render, redirect
from geopy.distance import geodesic
from folium import PolyLine
import networkx as nx  
from django.http import HttpResponse
from .models import Point
from .models import InformationCatalog
from .forms import InformationCatalogForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required, user_passes_test
import serial
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest
from django.core import serializers
from fcm_django.models import FCMDevice
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
import json
import serial.tools.list_ports
import threading    
from firebase_admin.messaging import Message, Notification
import firebase_admin
from firebase_admin import credentials
import time


cred = credentials.Certificate("myApp/static/js/evacuationrutes-firebase-adminsdk-2bwha-bc8374b34a.json")
firebase_admin.initialize_app(cred)



def is_admin(user):
    return user.is_superuser


def brigadistaPage(request):
    filter_by = request.GET.get('filter_by')
    faculty_id = request.GET.get('faculty_id')

    faculties = Faculty.objects.all()

    brigadistas = User.objects.filter(is_active=1, is_staff=1)

    if filter_by == 'faculty' and faculty_id:
        if faculty_id != 'all':
            brigadistas = brigadistas.filter(person__faculty_id=faculty_id)

    return render(request, 'myApp/brigadistaPage.html', {'brigadistas': brigadistas, 'faculties': faculties})


@csrf_exempt
@login_required(login_url='login')
def modifyInformationUser(request):
    user = request.user
    person = Person.objects.get(user=user)

    if request.method == 'POST':
        user.username = request.POST.get('username', user.username)
        user.email = request.POST.get('email', user.email)
        user.first_name = request.POST.get('first_name', user.first_name)
        user.last_name = request.POST.get('last_name', user.last_name)
        person.cellphoneNumber = request.POST.get('cellphoneNumber', person.cellphoneNumber)
        person.cubicle = request.POST.get('cubicle', person.cubicle)

        try:
            user.save()
            person.save()
            messages.success(request, 'Se actualizó la información exitosamente.')
            return redirect('modifyInformationUser')
        except Exception as e:
            print(f"Error al guardar: {e}")
            messages.error(request, 'No se pudo actualizar la información.')

    return render(request, 'myApp/modifyInformationUser.html', {'user': user, 'person': person})


class ContraseñaCortaError(Exception):
    def __init__(self):
        super().__init__('La contraseña es demasiado corta. Debe contener al menos 8 caracteres.')

class ContraseñaComúnError(Exception):
    def __init__(self):
        super().__init__('La contraseña es demasiado común, ingrese otra por favor.')

class ContraseñaNuméricaError(Exception):
    def __init__(self):
        super().__init__('La contraseña está completamente numérica, ingrese otra por favor.')

class ContraseñaNoCoincideError(Exception):
    def __init__(self):
        super().__init__('La contraseña nueva no coincide con la confirmación.')

class ContraseñaIncorrectaAnteriorError(Exception):
    def __init__(self):
        super().__init__('Su contraseña anterior fue ingresada incorrectamente. Por favor, ingrésela nuevamente.')

@csrf_exempt
@login_required(login_url='login')
def changePassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Actualiza la sesión del usuario
            messages.success(request, 'Contraseña cambiada exitosamente.')
            return redirect('modifyInformationUser')
        else:
            for field, errors in form.errors.items():
                for error in errors:
                    # Manejar excepciones personalizadas para errores de contraseña en español
                    try:
                        if 'This password is too short' in error:
                            raise ContraseñaCortaError()
                        elif 'This password is too common' in error:
                            raise ContraseñaComúnError()
                        elif 'This password is entirely numeric' in error:
                            raise ContraseñaNuméricaError()
                        elif 'The two password fields didn’t match' in error:
                            raise ContraseñaNoCoincideError()
                        elif 'Your old password was entered incorrectly' in error:
                            raise ContraseñaIncorrectaAnteriorError()
                    except (ContraseñaCortaError, ContraseñaComúnError, ContraseñaNuméricaError, ContraseñaNoCoincideError, ContraseñaIncorrectaAnteriorError) as e:
                        messages.error(request, str(e))
                    else:
                        pass
    else:
        form = PasswordChangeForm(user=request.user)
    return render(request, 'myApp/changePassword.html', {'form': form})

@csrf_exempt
@login_required(login_url='login')
def userManagement(request):
    persons = Person.objects.all()
    return render(request, 'myApp/userManagement.html', {'persons': persons})

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
def updateStateUser(request, account_id):
    account = User.objects.get(id=account_id)
    # Cambiar el estado del usuario entre habilitado y deshabilitado
    account.is_active = not account.is_active
    account.save()
    return HttpResponseRedirect('/userManagement/?message=El+estado+se+actualizó.')

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def addFaculty(request):
    faculties = Faculty.objects.all()

    if request.method == 'POST':
        faculty_id = request.POST.get('faculty_id')
        name = request.POST.get('name')
        acronym = request.POST.get('acronym')

        if faculty_id:
            existing_faculty = Faculty.objects.get(id=faculty_id)
            existing_faculty.name = name
            existing_faculty.acronym = acronym
            existing_faculty.save()
        else:
            new_faculty = Faculty(name=name, acronym=acronym)
            new_faculty.save()

        return redirect('addFaculty')

    return render(request, 'myApp/addFaculty.html', {'faculties': faculties})

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def addPoints(request):
    faculties = Faculty.objects.all()
    points = Point.objects.all()

    if request.method == 'POST':
        id = request.POST.get('point_id')
        name = request.POST.get('name')
        longitude = request.POST.get('longitude')
        latitude = request.POST.get('latitude')
        faculty_id = request.POST.get('faculty')
        status = request.POST.get('status')
        description = request.POST.get('description')

        if status == 'on':
            status = '1'
        else:
            status = '0'

        if status == '' or status not in ['0', '1']:
            return HttpResponseBadRequest("El estado del punto no está especificado.")

        faculty = Faculty.objects.get(id=faculty_id) if faculty_id else None

        if id:
            existing_point = get_object_or_404(Point, id=id)
            existing_point.name = name
            existing_point.longitude = longitude
            existing_point.latitude = latitude
            existing_point.faculty = faculty
            existing_point.status = bool(int(status))
            existing_point.description = description
            existing_point.save()
        else:
            new_point = Point(name=name, longitude=longitude, latitude=latitude,
                              faculty=faculty, status=bool(int(status)), description=description)
            new_point.save()

        return redirect('addPoints')

    for point in points:
        point.status_label = "Activado" if point.status else "Desactivado"

    return render(request, 'myApp/addPoint.html', {'points': points, 'faculties': faculties})

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
def activate_point(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        status = request.POST.get('status')

        point = get_object_or_404(Point, id=id)
        point.status = not bool(int(status))
        point.save()

        return JsonResponse({'success': True, 'new_status': point.status_label})

    return JsonResponse({'success': False})


@csrf_exempt
def signup(request):
    if request.method == 'POST':
        try:
            dni = request.POST.get('dni')
            name = request.POST.get('name')
            lastname = request.POST.get('lastname')
            cellphonenumber = request.POST.get('cellphonenumber')
            cubicle = request.POST.get('cubicle')
            username = request.POST.get('username')
            raw_password = request.POST.get('password')
            email = request.POST.get('email')
            role_id = request.POST.get('role')
            faculty_id = request.POST.get('faculty')

            if role_id not in ['1', '2', '3']:
                messages.error(request, 'Rol seleccionado no válido.')
                return redirect('signup')

            is_superuser = True if role_id == '1' else False
            is_staff = True if role_id == '2' else False
            faculty = Faculty.objects.get(pk=faculty_id)

            user = User.objects.create_user(
                username=username,
                password=raw_password,
                email=email,
                is_active=False,
                is_superuser=is_superuser,
                is_staff=is_staff,
                last_name=lastname,  
                first_name=name  
            )

            person = Person.objects.create(
                user=user,
                dni=dni,
                cellphoneNumber=cellphonenumber,
                cubicle=cubicle,
                faculty=faculty
            )

            if faculty_id:
                try:
                    faculty = Faculty.objects.get(id=faculty_id)
                    person.faculty = faculty
                    person.save()
                except Faculty.DoesNotExist:
                    messages.warning(request, 'Facultad seleccionada no válida.')

            messages.success(request, 'Usuario registrado exitosamente.')
            return redirect('home')

        except Exception as e:
            messages.error(request, f'Error al registrar usuario: {str(e)}')
            return redirect('signup')

    roles = [
        {'id': 1, 'name': 'Administrador'},
        {'id': 2, 'name': 'Brigadista'},
        {'id': 3, 'name': 'Docente'},
    ]

    faculties = Faculty.objects.all()

    return render(request, 'myApp/signup.html', {'roles': roles, 'faculties': faculties})

@csrf_exempt
def welcome(request):
    user = request.user
    context = {'user': user}
    return render(request, 'myApp/welcome.html', context)





from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def customLogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None and user.is_active:
            login(request, user)

            if user.is_superuser:
                return redirect('adminPage')  
            elif user.is_staff:
                return redirect('home')  
            else:
                return redirect('welcome')   
        elif user is not None and not user.is_active:
            return redirect('welcome')
        else:
            return redirect('welcome')
    return render(request, 'myApp/login.html')

@csrf_exempt
def home(request):
    
    #iniciar_deteccion_sismo()
    prueba()
    safe_points = Point.objects.filter(name__iexact='Punto Seguro')
    all_points = Point.objects.all()
    all_points = all_points.filter(status=True)
    inicio = None
    distancia_minima = None
    fin = None

    user_location = request.session.get('user_location', None)
    #user_location = {'latitud': '-4.034753', 'longitud': '-79.203903'}

    # request.session.get('user_location', None)
    #user_location = {'latitud': '-4.03784', 'longitud': '-79.20367'}
    # {'latitud': '-4.032719', 'longitud': '-79.201510'}-4.03562,-79.20418

    if user_location:
        map_center = [float(user_location['latitud']),
                      float(user_location['longitud'])]
    else:
        map_center = [safe_points[0].latitude, safe_points[0].longitude]

    my_map = folium.Map(location=map_center, zoom_start=30)

    if user_location:
        try:
            graph = crear_grafo(all_points, distance_threshold=30)
            inicio, fin = buscar_nodo_mas_cercano(
                user_location, graph, safe_points)
            ruta = dijkstra_ruta_mas_corta(graph, inicio, fin)
            path_coordinates = [
                (graph.nodes[node]['latitude'], graph.nodes[node]['longitude']) for node in ruta]

            # mostrar usuario dentro del mapa
            if user_location:
                user_lat = float(user_location['latitud'])
                user_lon = float(user_location['longitud'])

                user_icon = folium.Icon(color='blue', icon='glyphicon-user')

                folium.Marker(
                    location=[user_lat, user_lon],
                    popup="¡Usted está aquí!",
                    icon=user_icon
                ).add_to(my_map)

            if inicio:
                folium.PolyLine(
                    locations=[
                        [float(user_location['latitud']),
                         float(user_location['longitud'])],
                        [graph.nodes[inicio]['latitude'],
                            graph.nodes[inicio]['longitude']]
                    ],
                    color='blue', weight=5
                ).add_to(my_map)

            route_coordinates = [(node.latitude, node.longitude)
                                 for node in ruta]
            PolyLine(route_coordinates, color='blue', weight=5).add_to(my_map)

        except nx.NetworkXNoPath:
            print("No hay un camino válido dentro del límite de distancia establecido.")

    for point in safe_points:
        marker_color = 'green' if point.name.lower() == 'punto seguro' else 'blue'
        folium.Marker(
            location=[point.latitude, point.longitude],
            popup=point.name,
            icon=folium.Icon(color=marker_color)
        ).add_to(my_map)


#    for point in all_points:

 #       if point.name.lower() != 'punto seguro':
  #          marker_color = 'blue'
   #         folium.Marker(
    #            location=[point.latitude, point.longitude],
     #           popup=point.name,
      #          icon=folium.Icon(color=marker_color)
       #     ).add_to(my_map)

   # for coord in path_coordinates:
    #    folium.Marker(
     #       location=[coord[0], coord[1]],
      #      popup="Point on Path",
       #     icon=folium.Icon(color='orange')
    # ).add_to(my_map)

    map_html = my_map.get_root().render()

    return render(request, 'myApp/home.html', {'map_html': map_html})

@csrf_exempt
def notificacion(request):
    return render(request, 'myApp/notificacion.html')

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def mapaAdmin(request):
    points = Point.objects.all()
    points = points.filter(status=True)
    graph = crear_grafo(points, distance_threshold=30)

    my_map = folium.Map(
        location=[points[0].latitude, points[0].longitude],
        zoom_start=25,
    )

    for node in graph.nodes:
        folium.Marker(
            location=[node.latitude, node.longitude],
            popup=node.name
        ).add_to(my_map)

    for edge in graph.edges:
        folium.PolyLine(
            locations=([(edge[0].latitude, edge[0].longitude),
                       (edge[1].latitude, edge[1].longitude)]),
            color="blue",
            weight=2.5,
            opacity=1
        ).add_to(my_map)

    map_html = my_map.get_root().render()

    return render(request, 'myApp/mapaAdmin.html', {'map_html': map_html})

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def adminPage(request):
    user = request.user
    person = user.person if hasattr(user, 'person') else None
    return render(request, 'myApp/adminPage.html', {'user': user, 'person': person})

from django.contrib.auth import logout
from django.views.decorators.http import require_POST
@csrf_exempt
@require_POST
def logoutView(request):
    logout(request)
    return redirect('login')

def index_ubicacion(request):
    return render(request, 'myApp/home.html')


def obtener_ubicacion(request):
    latitud = request.GET.get('latitud', None)
    longitud = request.GET.get('longitud', None)

    if latitud is not None and longitud is not None:
        ubicacion = {'latitud': latitud, 'longitud': longitud}
        request.session['user_location'] = ubicacion
        return JsonResponse(ubicacion)
    else:
        return JsonResponse({'error': 'No se proporcionaron datos de ubicación'})


def crear_grafo(points, distance_threshold):
    G = nx.Graph()

    for point in points:
        node_attributes = point.__dict__
        G.add_node(point, **node_attributes)

    for i in range(len(points)):
        for j in range(i + 1, len(points)):
            dist = geodesic((points[i].latitude, points[i].longitude),
                            (points[j].latitude, points[j].longitude)).meters
            if dist <= distance_threshold:
                G.add_edge(points[i], points[j], weight=dist)

    return G


def buscar_nodo_mas_cercano(inicio, graph, puntos_seguros):
    coords = (float(inicio['latitud']), float(inicio['longitud']))

    min_distance = float('inf')
    closest_node = None

    for node, data in graph.nodes(data=True):
        node_coords = (data['latitude'], data['longitude'])
        distance = geodesic(coords, node_coords).meters

        if distance < min_distance:
            min_distance = distance
            closest_node = node

    punto_seguro_mas_cercano = min(puntos_seguros, key=lambda punto: geodesic(
        (graph.nodes[closest_node]['latitude'],
         graph.nodes[closest_node]['longitude']),
        (punto.latitude, punto.longitude)
    ).meters)

    return closest_node, punto_seguro_mas_cercano


def dijkstra_ruta_mas_corta(graph, inicio, fin):
    return nx.shortest_path(graph, source=inicio, target=fin, weight='weight')


# Pool
'''
def graph_view(request):
    # Obtén todos los puntos de la base de datos
    all_points = Point.objects.all()

    # Crea un grafo dirigido vacío
    G = nx.DiGraph()

    # Agrega nodos al grafo
    for point in all_points:
        G.add_node(point.name)

    # Agrega aristas al grafo (puedes personalizar esto según tus necesidades)
    for point in all_points:
        if point.faculty:
            G.add_edge(point.faculty.name, point.name)

    # Dibuja el grafo en una hoja en blanco
    plt.figure(figsize=(10, 6))
    pos = nx.spring_layout(G)  # Puedes cambiar el algoritmo de disposición según tus preferencias
    nx.draw(G, pos, with_labels=True, font_weight='bold', node_size=700, node_color='skyblue', edge_color='gray')

    # Guarda la imagen en un archivo
    plt.savefig('graph.png')

    # Devuelve la imagen al navegador
    with open('graph.png', 'rb') as image_file:
        response = HttpResponse(image_file.read(), content_type='image/png')
        response['Content-Disposition'] = 'inline; filename="graph.png"'
        return response'''






def graph_view(request):
    points = Point.objects.all()
    points_data = [point.to_dict() for point in points]
    print(points_data)
    return render(request, 'myApp/graph.html', {'points_data': points_data})



def informacion(request):
    informacion_items = InformationCatalog.objects.filter(status=True)
    return render(request, 'myApp/informacion.html', {'informacion_items': informacion_items})


@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def panel_informativo(request):
    informacion_items = InformationCatalog.objects.all()

    if request.method == 'POST':
        form = InformationCatalogForm(request.POST)

        if form.is_valid():
            informacion_id = request.POST.get('id')
            status = request.POST.get('status')

            # Verifica si el ID existe
            if informacion_id:
                information_instance = get_object_or_404(
                    InformationCatalog, pk=informacion_id)
            else:
                informacion_id = InformationCatalog.objects.count() + 1
                information_instance = InformationCatalog(pk=informacion_id)

            information_instance.title = form.cleaned_data['title']
            information_instance.description = form.cleaned_data['description']
            information_instance.status = status

            information_instance.save()

            return redirect('panel_informativo')  

        else:
            return JsonResponse({'success': False, 'errors': form.errors})

    else:
        form = InformationCatalogForm()

    return render(request, 'myApp/panelInformativo.html', {'informacion_items': informacion_items, 'form': form})

@csrf_exempt
@user_passes_test(is_admin, login_url='login')
@login_required(login_url='login')
def upload_emergency_plan(request):
    informacion_items = EmergencyPlan.objects.all()

    if request.method == 'POST':
        form = EmergencyPlanForm(request.POST)

        if form.is_valid():
            informacion_id = request.POST.get('id')
            status = request.POST.get('status')

            # Verifica si el ID existe
            if informacion_id:
                information_instance = get_object_or_404(
                    EmergencyPlan, pk=informacion_id)
            else:
                informacion_id = EmergencyPlan.objects.count() + 1
                information_instance = EmergencyPlan(pk=informacion_id)

            information_instance.name = form.cleaned_data['name']
            information_instance.link = form.cleaned_data['link']
            information_instance.status = status

            information_instance.save()

            return redirect('upload_emergency_plan')

        else:
            return JsonResponse({'success': False, 'errors': form.errors})

    else:
        form = EmergencyPlanForm()

    return render(request, 'myApp/emergencyPlan.html', {'informacion_items': informacion_items, 'form': form})

    
@csrf_exempt
@require_http_methods(['POST'])
def guardar_token(request):   

#    usuarios = User.objects.all()
    body = request.body.decode('utf-8')
    bodyDict = json.loads(body)

    token = bodyDict['token']
    existe = FCMDevice.objects.filter(registration_id = token, active=True)
    if len(existe) > 0:
        return HttpResponseBadRequest(json.dumps({'mensaje':'el token ya existe'}))
    dispositivo = FCMDevice()
    dispositivo.registration_id = token
    dispositivo.active = True

    try:
        dispositivo.save()
        print("guardado")
        return HttpResponse(json.dumps({'mensaje':'Token guardado'}))
    except:
        return HttpResponse(json.dumps({'mensaje':'token no guardado'}))
    
def iniciar_deteccion_sismo():
    hilo_deteccion_sismo = threading.Thread(target=deteccion_sismo)
    hilo_deteccion_sismo.daemon = True
    hilo_deteccion_sismo.start()
    

def deteccion_sismo():

    try:
        puerto_serial = '/dev/ttyUSB0'

        arduino = serial.Serial(puerto_serial, 9600, timeout=1)

        dispositivos = FCMDevice.objects.filter(active = True)

        
        while True:
            datos = arduino.readline().decode('utf-8').rstrip()

            try:
                valor = int(datos)
            except ValueError:
                # Si no se puede convertir a un entero, continuar con la próxima iteración del bucle
                continue

            if valor >= 6:
                    
            #enviar notificación
                    
                url_principal = "http://127.0.0.1:8000/"
                dispositivo = FCMDevice.objects.filter(active = True)
                dispositivo.send_message(
                Message(
                    notification=Notification(
                        title="Atención, sismo detectado de: " + str(valor), 
                        body="Haga clic para ver ruta de evacuación",
                    ),
                    data={"click_action": url_principal},  
                    )
                )
                valor = 0
                time.sleep(5)
                iniciar_deteccion_sismo()
                break
            print(datos)

    except KeyboardInterrupt:
        arduino.close()
        print("Conexión cerrada.")
    except serial.SerialException as e:
        print("Error al acceder al puerto serial:", e)

def prueba():
    url_principal = "http://127.0.0.1:8000"
    dispositivo = FCMDevice.objects.filter(active = True)
    dispositivo.send_message(
    Message(
        notification=Notification(
            title="Atención, sismo detectado de: ",
            body="Haga clic para ver ruta de evacuación",
        ),
        data={"click_action": url_principal},  
    )
)


def mostrarEnlace(request):
    informacion_items = EmergencyPlan.objects.filter(status=True)
    return render(request, 'myApp/mostrarEnlace.html', {'informacion_items': informacion_items})

from ckeditor.fields import RichTextField
from django.db import models
import math
from math import sqrt
from collections import defaultdict
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User





class Faculty(models.Model):
    name = models.CharField(max_length=100)
    acronym = models.CharField(max_length=20)

    def _str_(self):
        return f"{self.name} ({self.acronym})"


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    dni = models.CharField(max_length=10, unique=True, blank=False, null=False)
    cellphoneNumber = models.CharField(max_length=10, blank=False, null=False)
    cubicle = models.CharField(max_length=10, blank=False, null=False)
    faculty = models.ForeignKey(Faculty, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return f"{self.dni} - {self.cellphoneNumber} - {self.cubicle}"




class Point(models.Model):
    name = models.CharField(max_length=100)
    longitude = models.FloatField()
    status = models.BooleanField(default=True)
    latitude = models.FloatField()
    description = models.CharField(max_length=100, default='Sin descripción')
    faculty = models.ForeignKey(
        Faculty, on_delete=models.CASCADE, related_name='points', null=True, blank=True)

    def toggle_status(self):
        self.status = not self.status
        self.save()

    def _str_(self):
        return f"{self.name} - longitude: {self.longitude} - Latitude: {self.latitude} - description: {self.description} - Facultad: {self.faculty} - Estado: {'Activado' if self.status else 'Desactivado'}"
    # POOOL BORRAR

    def to_dict(self):
        return {
            'name': self.name,
            'longitude': self.longitude,
            'latitude': self.latitude,
            'status': self.status,
            'description': self.description,
            'faculty': str(self.faculty),
        }


class InformationCatalog(models.Model):
    STATUS_CHOICES = (
        (True, 'Activo'),
        (False, 'Inactivo'),
    )

    title = models.CharField(max_length=50)
    description = RichTextField()
    status = models.BooleanField(default=True)

    def _str_(self):
        return f"{self.title} - {self.description[:50]}"


class EmergencyPlan(models.Model):
    STATUS_CHOICES = (
        (True, 'Activo'),
        (False, 'Inactivo'),
    )

    name = models.CharField(max_length=40, null=True, blank=True)
    link = models.CharField(max_length=110, null=True, blank=True)
    status = models.BooleanField(default=True)

    def _str_(self):
        return f"{self.name} - {self.name}"
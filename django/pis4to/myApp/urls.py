from . import views
from django.urls import path, include, re_path
from django.urls import re_path as url
from django.contrib.auth.views import LogoutView
from .views import userManagement, updateStateUser, guardar_token



urlpatterns = [
    path("", views.home, name="home"),
    path("mapaAdmin/", views.mapaAdmin, name="mapaAdmin"),
    path("login/", views.customLogin, name="login"),
    path("welcome/", views.welcome, name="welcome"),
    path('signup/', views.signup, name='signup'),
    path('addPoint/', views.addPoints, name='addPoints'),
    path('addFaculty/', views.addFaculty, name='addFaculty'),
    path('adminPage/', views.adminPage, name='adminPage'),
    path('userManagement/', userManagement, name='userManagement'),
    path('updateStateUser/<int:account_id>/',
         updateStateUser, name='updateStateUser'),
    path('obtener_ubicacion/', views.obtener_ubicacion, name='obtener'),
    path('index/', views.index_ubicacion, name='index'),
    path('graph/', views.graph_view, name='graph_view'),
    path('panel_informativo/', views.panel_informativo, name='panel_informativo'),
    path('informacion/', views.informacion, name='informacion'),
    path('upload_emergency_plan/', views.upload_emergency_plan,
         name='upload_emergency_plan'),
    path('brigadistaPage/', views.brigadistaPage, name='brigadistaPage'),
    path('logout/', views.logoutView, name='logout'),
    path('guardar-token/', guardar_token, name ='guardar-token'),
    path('modifyInformationUser/', views.modifyInformationUser, name ='modifyInformationUser'),
    path('changePassword/', views.changePassword, name='changePassword'),
    path('documents/', views.mostrarEnlace, name='documents'),


]
